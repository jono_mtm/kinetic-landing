import styled from "@emotion/styled"
import { motion } from "framer-motion"
import React from "react"
import { margins } from "../config/margins"
import Form from "./Form"

const Content = () => {
  return (
    <Wrapper
      initial={{ opacity: 0, transform: "translateY(5px)" }}
      animate={{ opacity: 1, transform: "translateY(0px)" }}
      transition={{ duration: 1, delay: 1 }}
    >
      <h2 className="mb-2 text-4xl font-extrabold sm:text-6xl text-green-400">
        One System,
      </h2>
      <h2 className="text-5xl font-extrabold sm:text-6xl">
        Infinite Solutions.
      </h2>
      <p className="mt-7 text-lg leading-7 text-indigo-200">
        <em>Kinetics</em> is a Saas toolkit that manages your enterprise
        technology systems {"&"} services. Start your technology transformation
        with <em>Kinetics</em>
        today.
      </p>
      <Form />
    </Wrapper>
  )
}

export default Content

const Wrapper = styled(motion.div)`
  position: fixed;
  z-index: 2;
  color: white;
  width: ${margins.leftPanel};
  margin-left: ${`calc(${margins.leftPanel} * 0.3125)`};
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: flex-start;
  flex-direction: column;
`
