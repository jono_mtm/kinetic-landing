import styled from "@emotion/styled"
import { motion } from "framer-motion"
import React from "react"
import Form from "../Form"

const Content = () => {
  return (
    <Wrapper
      initial={{ opacity: 0, transform: "translateY(5px)" }}
      animate={{ opacity: 1, transform: "translateY(0px)" }}
      transition={{ duration: 1, delay: 1 }}
    >
      <h2 className="mb-2 text-2xl font-extrabold sm:text-6xl text-green-400">
        One System,
      </h2>
      <h2 className="text-4xl font-extrabold sm:text-6xl">
        Infinite Solutions.
      </h2>
      <div>
        <p className="mt-7 text-lg leading-7 text-indigo-200">
          <em>Kinetics</em> is a modular software suite of affordable technology
          that enables accessable tailored solutions for all types businesses.
          If you want to digitalise your business in any way, <em>Kinetics</em>{" "}
          can do it.
        </p>
      </div>
      <Form />
    </Wrapper>
  )
}

export default Content

const Wrapper = styled(motion.div)`
  color: white;
  text-align: center;
  margin: 0 3rem;
  margin-bottom: 3rem;
  display: flex;
  flex-direction: column;
  align-items: center;
`
