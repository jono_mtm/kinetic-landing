import { graphql, StaticQuery } from "gatsby"
import React from "react"
import { Helmet } from "react-helmet"

const Metadata: React.FC<{
  title: string
  desc: string
  //   banner?: string
  pathname?: string
  meta?: { firstPublicationDate?: string; lastPublicationDate?: string }
}> = ({ title, desc, /*banner*/ pathname, meta }) => {
  return (
    <StaticQuery
      query={`${query}`}
      render={({
        site: {
          buildTime,
          siteMetadata: {
            siteUrl,
            defaultTitle,
            defaultDescription,
            defaultBanner,
            headline,
            siteLanguage,
            author,
          },
        },
      }) => {
        const seo = {
          title: title || defaultTitle,
          description: desc || defaultDescription,
          //     image: `${siteUrl}${banner || defaultBanner}`,
          url: `${siteUrl}${pathname || ""}`,
        }

        // schema.org in JSONLD format
        // https://developers.google.com/search/docs/guides/intro-structured-data
        // You can fill out the 'author', 'creator' with more data or another type (e.g. 'Organization')

        const schemaOrgWebPage = {
          "@context": "http://schema.org",
          "@type": "WebPage",
          url: siteUrl,
          headline,
          inLanguage: siteLanguage,
          mainEntityOfPage: siteUrl,
          description: defaultDescription,
          name: defaultTitle,
          author: {
            "@type": "Person",
            name: author,
          },
          copyrightHolder: {
            "@type": "Person",
            name: author,
          },
          copyrightYear: "2020",
          creator: {
            "@type": "Person",
            name: author,
          },
          publisher: {
            "@type": "Person",
            name: author,
          },
          datePublished: "2020-12-19T10:30:00+01:00",
          dateModified: buildTime,
          image: {
            "@type": "ImageObject",
            url: `${siteUrl}${defaultBanner}`,
          },
        }

        return (
          <>
            <Helmet title={seo.title}>
              <html lang={siteLanguage} />
              <meta name="description" content={seo.description} />
              {/* <meta name="image" content={seo.image} /> */}
              {/* <meta
                                name="gatsby-starter"
                                content="Gatsby Starter Prismic"
                            /> */}
              {/* Insert schema.org data conditionally (webpage/article) + everytime (breadcrumbs) */}
              <script type="application/ld+json">
                {JSON.stringify(schemaOrgWebPage)}
              </script>
            </Helmet>
          </>
        )
      }}
    />
  )
}

export default Metadata

const query = graphql`
  query SEO {
    site {
      buildTime(formatString: "YYYY-MM-DD")
      siteMetadata {
        siteUrl
        defaultTitle: title
        defaultDescription: description
        #   defaultBanner: banner
        headline
        siteLanguage
        ogLanguage
        author
      }
    }
  }
`
