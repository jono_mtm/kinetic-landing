import styled from "@emotion/styled"
import React from "react"
import Logo from "./Logo"

const Header = () => {
  return (
    <Wrapper>
      <Logo />
    </Wrapper>
  )
}

export default Header

const Wrapper = styled.div`
  display: flex;
  width: 100vw;
  padding: 40px 0;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 12;
  justify-content: center;
  align-items: center;
`
