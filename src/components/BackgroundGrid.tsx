import React, { useRef, useLayoutEffect, useEffect } from "react"
import { motion, useAnimation } from "framer-motion"
import styled from "@emotion/styled"
import { margins } from "../config/margins"

const BackgroundGrid = ({ delayPerPixel = 0.0008, numItems = 90 }) => {
  const originOffset = useRef({ top: 0, left: 0 })
  const controls = useAnimation()

  useEffect(() => {
    controls.start("visible")
  }, [controls])

  return (
    <Wrapper initial="hidden" animate={controls} variants={{}}>
      {Array.from({ length: numItems }).map((_, i) => (
        <GridItem
          key={i}
          i={i}
          originIndex={0}
          delayPerPixel={delayPerPixel}
          originOffset={originOffset}
        />
      ))}
    </Wrapper>
  )
}

const GridItem = ({ delayPerPixel, i, originIndex, originOffset }) => {
  const delayRef = useRef(0)
  const offset = useRef({ top: 0, left: 0 })
  const ref = useRef<any>()

  useLayoutEffect(() => {
    const element = ref.current
    if (!element) return

    offset.current = {
      top: element.offsetTop,
      left: element.offsetLeft,
    }

    if (i === originIndex) {
      originOffset.current = offset.current
    }
  }, [delayPerPixel, i, originIndex, originOffset])

  useEffect(() => {
    const dx = Math.abs(offset.current.left - originOffset.current.left)
    const dy = Math.abs(offset.current.top - originOffset.current.top)
    const d = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2))
    delayRef.current = d * delayPerPixel
  }, [delayPerPixel, originOffset])

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      // transition={{
      //   repeat: Infinity,
      //   duration: 1 + Math.random() * 2,
      //   repeatType: "mirror",
      // }}
    >
      <Box ref={ref!} variants={itemVariants} custom={delayRef} />
    </motion.div>
  )
}

const itemVariants = {
  hidden: {
    opacity: 0,
  },
  visible: (delayRef) => ({
    opacity: Math.random() * 0.5 * 0.2,
    transition: {
      delay: delayRef.current,
    },
  }),
}

const Box = styled(motion.div)`
  margin: ${`calc(${margins.rightPanel} * 0.0325)`};
  display: inline-block;
  height: ${`calc(${margins.rightPanel} * 0.25)`};
  width: ${`calc(${margins.rightPanel} * 0.25)`};
  background-color: black;
  border-radius: 4px;
`

export default BackgroundGrid

const Wrapper = styled(motion.div)`
  display: flex;
  justify-content: center;
  align-content: center;
  flex-wrap: wrap;
`
