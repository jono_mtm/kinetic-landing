export const margins = {
  leftPanel: "40vw",
  rightPanel: "30vw",
  mobile: {
    gridGutter: "0.5rem",
    boxWidth: "15vh",
  },
}
