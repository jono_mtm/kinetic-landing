import styled from "@emotion/styled"
import React from "react"
import Logo from "../Logo"
import MobileGrid from "./MobileGrid"

const MobileHeader = () => {
  return (
    <>
      <Wrapper>
        <Logo />
        <MobileGrid />
      </Wrapper>
    </>
  )
}

export default MobileHeader

const Wrapper = styled.div`
  display: flex;
  width: 100vw;
  padding: 3rem;
  top: 0;
  z-index: 12;
  align-items: center;
  flex-direction: column;
`
