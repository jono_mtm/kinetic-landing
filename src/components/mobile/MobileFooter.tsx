import styled from "@emotion/styled"
import { motion } from "framer-motion"
import React from "react"
import SurfaceSrc from "../../images/surface.svg"
import BusinessLogos from "../BusinessLogos"

const Footer = () => {
  return (
    <>
      <motion.div
        initial={{ opacity: 0, transform: "translateY(20px)" }}
        animate={{ opacity: 1, transform: "translateY(0px)" }}
        transition={{ duration: 2 }}
      >
        <Surface src={SurfaceSrc} alt="Surface Background" />
      </motion.div>
      <Wrapper
        initial={{ opacity: 0, transform: "translateY(10px)" }}
        animate={{ opacity: 1, transform: "translateY(0px)" }}
        transition={{ duration: 1.5, delay: 0.4 }}
      >
        <BusinessLogos />
      </Wrapper>
    </>
  )
}

export default Footer

const Wrapper = styled(motion.div)`
  width: 100vw;
  height: 70px;
  display: flex;
  justify-content: center;
  padding-top: 35px;
  z-index: 2;
  & > * {
    max-width: 10vw;
    margin: 5vw;
  }
`

export const Surface = styled.img`
  width: 100vw;
  position: fixed;
  top: calc(100% + 6rem - 70px);
`
