import React from "react"
import logoSrcWhite from "../images/kinetics-logo-white.png"

const Logo = () => {
  return <img src={logoSrcWhite} alt="Logo" width={110} />
}

export default Logo
