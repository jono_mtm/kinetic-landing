import styled from "@emotion/styled"
import React from "react"
import FollowMeLogoSrc from "../images/logos/followme-logo.svg"
import MmuchoLogoSrc from "../images/logos/mmucho-logo.svg"
import TangLogoSrc from "../images/logos/tang-logo.svg"
import VtlogisticsLogoSrc from "../images/logos/vtlogistics-logo.svg"
import YfaLogoSrc from "../images/logos/yfa-logo.svg"

const businesses = [
  { name: "YFA Logo", logo: { src: YfaLogoSrc } },
  { name: "Tang Logo", logo: { src: TangLogoSrc } },
  { name: "Mmucho Logo", logo: { src: MmuchoLogoSrc } },
  { name: "VTLogistics Logo", logo: { src: VtlogisticsLogoSrc } },
  { name: "Follow Me Logo", logo: { src: FollowMeLogoSrc } },
]

const BusinessLogos = () => {
  return (
    <>
      {businesses.map((business) => (
        <BusinessLogo
          key={business.name}
          src={business.logo.src}
          alt={business.name}
        />
      ))}
    </>
  )
}

export default BusinessLogos

const BusinessLogo = styled.img`
  height: 25px;
  margin: 22px;
  /* opacity: 0.3; */
`
