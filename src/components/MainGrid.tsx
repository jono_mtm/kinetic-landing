import styled from "@emotion/styled"
import { motion, useAnimation } from "framer-motion"
import React, { useEffect, useLayoutEffect, useRef } from "react"
import { margins } from "../config/margins"
import { tiles } from "../config/tiles"

const MainGrid = ({ delayPerPixel = 0.01 }) => {
  const originOffset = useRef({ top: 0, left: 0 })
  const controls = useAnimation()

  useEffect(() => {
    controls.start("visible")
  }, [controls])

  return (
    <Wrapper initial="hidden" animate={controls} variants={{}}>
      {tiles.map((tile, i) => (
        <GridItem
          key={i}
          i={i}
          src={tile}
          originIndex={0}
          delayPerPixel={delayPerPixel}
          originOffset={originOffset}
        />
      ))}
    </Wrapper>
  )
}

const GridItem = ({ src, delayPerPixel, i, originIndex, originOffset }) => {
  const delayRef = useRef(0)
  const offset = useRef({ top: 0, left: 0 })
  const ref = useRef<any>()

  useLayoutEffect(() => {
    const element = ref.current
    if (!element) return

    offset.current = {
      top: element.offsetTop,
      left: element.offsetLeft,
    }

    if (i === originIndex) {
      originOffset.current = offset.current
    }
  }, [delayPerPixel, i, originIndex, originOffset])

  useEffect(() => {
    const dx = Math.abs(offset.current.left - originOffset.current.left)
    const dy = Math.abs(offset.current.top - originOffset.current.top)
    const d = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2))
    delayRef.current = d * delayPerPixel
  }, [delayPerPixel, originOffset])

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      transition={{
        repeat: Infinity,
        delay: Math.random() * 1,
        duration: 3 + Math.random() * 8,
        repeatType: "mirror",
      }}
      // whileHover={{ opacity: 1 }}
    >
      <Box src={src} ref={ref!} variants={itemVariants} custom={delayRef} />
    </motion.div>
  )
}

const itemVariants = {
  hidden: {
    opacity: 0,
    borderColor: "transparent",
  },
  visible: (delayRef) => ({
    opacity: 1,
    transition: { delay: delayRef.current },
  }),
}

const Box = styled(motion.div)<{ src: string }>`
  background: ${(props) => `url('${props.src}')`};
  background-position: center;
  background-size: contain;
  margin: ${`calc(${margins.rightPanel} * 0.0325)`};
  display: inline-block;
  height: ${`calc(${margins.rightPanel} * 0.25)`};
  width: ${`calc(${margins.rightPanel} * 0.25)`};
  /* background: white; */
  border-radius: 4px;
`

export default MainGrid

const Wrapper = styled(motion.div)`
  display: flex;
  justify-content: center;
  align-content: center;
  flex-wrap: wrap;
`
