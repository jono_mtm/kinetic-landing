import { yupResolver } from "@hookform/resolvers/yup"
import React, { useState } from "react"
import { useForm } from "react-hook-form"
import * as yup from "yup"

const validationSchema = yup.object().shape({
  email: yup.string().email().required(),
})

const Form = () => {
  const { register, handleSubmit, errors, setError } = useForm({
    resolver: yupResolver(validationSchema),
  })

  const [isSubmitting, setSubmitting] = useState<boolean>(false)

  const onSubmit = async (values) => {
    setSubmitting(true)

    if (values) {
      const body = {
        "form-name": "contact",
        ...values,
      }

      const res = await fetch("/", {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        body: encode(body),
      })

      setSubmitting(false)

      if (res.status === 200) {
        alert("submitted")
      } else {
        setSubmitting(false)
        setError("email", {
          message: `${res.status} Please check Error!`,
        })
      }
    }
  }

  return (
    <div>
      <form
        className="mt-8 sm:flex"
        onSubmit={handleSubmit(onSubmit)}
        name="contact"
        method="POST"
        data-netlify="true"
      >
        <label htmlFor="emailAddress" className="sr-only">
          Email address
        </label>
        <input
          id="emailAddress"
          name="email"
          type="email"
          autoComplete="email"
          required
          ref={register}
          className="w-full px-5 py-3 placeholder-gray-500 focus:ring-green-500 focus:border-green-500 text-indigo-500 sm:max-w-xs border-gray-300 rounded-md"
          placeholder="Enter your email"
        />
        <div className="mt-3 rounded-md shadow sm:mt-0 sm:ml-3 sm:flex-shrink-0">
          <button
            type="submit"
            className="w-full flex items-center justify-center px-5 py-3 border border-transparent text-base font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500"
          >
            Notify me
          </button>
        </div>
      </form>
    </div>
  )
}

export default Form

const encode = (data) => {
  return Object.keys(data)
    .map((key) => encodeURIComponent(key) + "=" + encodeURIComponent(data[key]))
    .join("&")
}
