import styled from "@emotion/styled"
import { motion } from "framer-motion"
import React, { useLayoutEffect, useRef } from "react"
import { margins } from "../../config/margins"
import { tiles } from "../../config/tiles"

const MobileGrid = () => {
  const originOffset = useRef({ top: 0, left: 0 })

  return (
    <Slideshow>
      <Wrapper>
        {tiles.map((tile, i) => (
          <GridItem
            key={i}
            src={tile}
            i={i}
            originIndex={0}
            originOffset={originOffset}
          />
        ))}
      </Wrapper>
    </Slideshow>
  )
}

const GridItem = ({ src, i, originIndex, originOffset }) => {
  const offset = useRef({ top: 0, left: 0 })
  const ref = useRef<any>()

  useLayoutEffect(() => {
    const element = ref.current
    if (!element) return

    offset.current = {
      top: element.offsetTop,
      left: element.offsetLeft,
    }

    if (i === originIndex) {
      originOffset.current = offset.current
    }
  }, [i, originIndex, originOffset])

  return (
    <motion.div
    // initial={{ opacity: 0 }}
    // animate={{ opacity: 1 }}
    // exit={{ opacity: 0 }}
    // transition={{
    //   repeat: Infinity,
    //   delay: Math.random() * 0.5,
    //   duration: 3 + Math.random() * 3,
    //   repeatType: "mirror",
    // }}
    >
      <Box src={src} ref={ref!} variants={itemVariants} />
    </motion.div>
  )
}

const itemVariants = {
  hidden: {
    opacity: 0,
    borderColor: "transparent",
  },
  visible: {
    opacity: 1,
  },
}

const Box = styled(motion.div)<{ src: string }>`
  background: ${(props) => `url('${props.src}')`};
  background-position: center;
  background-size: contain;
  margin: ${margins.mobile.gridGutter};
  display: inline-block;
  height: ${margins.mobile.boxWidth};
  width: ${margins.mobile.boxWidth};
  border-radius: 4px;
`

export default MobileGrid

const Wrapper = styled.div`
  padding-top: 3rem;
  display: flex;
  justify-content: center;
  align-content: center;
  animation: slideshow 10s linear infinite;
`

const Slideshow = styled.div`
  max-width: 100vw;
  position: relative;
  overflow: hidden;
`
