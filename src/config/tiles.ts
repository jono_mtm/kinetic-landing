import one from "../images/tiles/1.png"
import two from "../images/tiles/2.png"
import three from "../images/tiles/3.png"
import four from "../images/tiles/4.png"
import five from "../images/tiles/5.png"
import six from "../images/tiles/6.png"
import seven from "../images/tiles/7.png"
import eight from "../images/tiles/8.png"
import nine from "../images/tiles/9.png"

export const tiles = [one, two, three, four, five, six, seven, eight, nine]
