import styled from "@emotion/styled"
import React from "react"
import { margins } from "../config/margins"
import BackgroundGrid from "./BackgroundGrid"
import MainGrid from "./MainGrid"
import Sky from "./Sky"

const Background = () => {
  return (
    <>
      <Wrapper>
        <div />
        <RightColumn>
          <MainGrid />
        </RightColumn>
      </Wrapper>
      <BackgroundGridWrapper>
        <BackgroundGrid />
      </BackgroundGridWrapper>
      <Sky />
    </>
  )
}

export default Background

const RightColumn = styled.div`
  width: ${margins.rightPanel};
  position: relative;
  margin-right: ${`calc(${margins.rightPanel} * 0.454)`};
`

const Wrapper = styled.div`
  position: fixed;
  width: 100vw;
  height: 100vh;
  top: 0;
  left: 0;
  z-index: 1;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

const BackgroundGridWrapper = styled.div`
  position: fixed;
  width: 105vw;
  height: 100vh;
  top: 0;
  left: 0;
  z-index: 0;
  display: flex;
  justify-content: flex-start;
  align-items: center;
`
