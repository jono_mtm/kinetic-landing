import styled from "@emotion/styled"

const breakpoints = {
  xl: "1800px",
  l: "1400px",
  m: "1200px",
  sm: "1000px",
}

export const Mobile = styled.div`
  display: none;
  @media (max-width: ${breakpoints.sm}) {
    display: block;
  } ;
`

export const Desktop = styled.div`
  display: none;
  @media (min-width: ${breakpoints.sm}) {
    display: block;
  }
`
