import styled from "@emotion/styled"
import { motion } from "framer-motion"
import React from "react"

const MobileSky = () => {
  return (
    <Wrapper>
      {[...Array(20)].map((star, i) => (
        <Star
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          exit={{ opacity: 0 }}
          transition={{
            repeat: Infinity,
            duration: 2 + Math.random() * 2,
            repeatType: "mirror",
          }}
          key={i}
          left={Math.random() * 100}
          right={Math.random() * 100}
        />
      ))}
    </Wrapper>
  )
}

export default MobileSky

const Star = styled(motion.div)<{ left: number; right: number }>`
  height: 2px;
  width: 2px;

  margin: 0;
  padding: 0;
  background-color: white;
  position: fixed;
  overflow: hidden;

  left: ${(props) => `${props.left}vw`};
  top: ${(props) => `${props.right}vh`};
`
const Wrapper = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100%;
  z-index: -1;
  background: radial-gradient(
    273.83% 273.83% at 50% 237.39%,
    #0da635 53%,
    #3232c0 67.71%,
    #12033c 89.06%
  );
  background-size: cover;
`
