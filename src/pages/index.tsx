import React from "react"
import Background from "../components/Background"
import Content from "../components/Content"
import Footer from "../components/Footer"
import Header from "../components/Header"
import Metadata from "../components/Metadata"
import MobileContent from "../components/mobile/MobileContent"
import MobileFooter from "../components/mobile/MobileFooter"
import MobileHeader from "../components/mobile/MobileHeader"
import MobileSky from "../components/mobile/MobileSky"
import { Desktop, Mobile } from "../components/responsive"

const Home = () => {
  const renderDesktop = () => (
    <Desktop>
      <Header />
      <Content />
      <Background />
      <Footer />
    </Desktop>
  )

  const renderMobile = () => (
    <Mobile>
      <MobileHeader />
      <MobileContent />
      <MobileSky />
      <MobileFooter />
    </Mobile>
  )

  const desktop = renderDesktop()
  const mobile = renderMobile()

  return (
    <>
      <Metadata title="Kinetics" desc="One System, Infinite Solutions" />
      {desktop}
      {mobile}
    </>
  )
}

export default Home
